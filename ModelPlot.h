//---------------------------------------------------------------------------

#ifndef ModelPlotH
#define ModelPlotH
//---------------------------------------------------------------------------
#include "KbPlot.h"
#include "GLWidget.h"
class ModelPlot
{
public:
    ModelPlot(GLWidget* view) : gl(view), p(0), ds(0), ds1(0), s(0), s1(0) {}
    ~ModelPlot()
    {
    	v.clear();
        delete ds;
        delete ds1;
        delete s;
        delete s1;
  		delete p;
        strM.clear();
    }

public:
	void CreateOption1();
    void CreateOption2();
    void CreateOption3();
    void CreateOption4();
private:
	void Clear();

	KbPlot *p;
    std::vector<Txy> v;
    DataSet *ds;
    DataSet *ds1;
    Style *s;
	Style *s1;
    std::vector<string>  strM;
    GLWidget *gl;
};
#endif
