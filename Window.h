/*
	http://msdn.microsoft.com/en-us/library/windows/desktop/aa383751(v=vs.85).aspx
	WinDataTypes:

		HINSTANCE - A handle to an instance. This is the base address of the module in memory.
		HACCEL - A handle to an accelerator table.
		LPSTR - A pointer to a null-terminated string of 8-bit Windows (ANSI) characters.

		http://msdn.microsoft.com/en-us/library/windows/desktop/ms645526(v=vs.85).aspx
		A keyboard accelerator (or, simply, accelerator) is a keystroke or combination of keystrokes that generates a WM_COMMAND or WM_SYSCOMMAND message for an application.


*/

 /*
  * ���������� ���������� � ����� Win API
  * http://msdn.microsoft.com/en-us/library/windows/desktop/aa383751(v=vs.85).aspx
  *
  * HWND - A handle to a window.
  * LPARAM - A message parameter.
  * HDC - A handle to a device context (DC).
  *	    http://msdn.microsoft.com/en-us/library/windows/desktop/dd183560(v=vs.85).aspx
  *		There are four types of DCs: display, printer, memory (or compatible), and information. Each type serves a specific purpose, as described in the following table.
  *		Display - Supports drawing operations on a video display.
  *	WPARAM - A message parameter.
  * HGLRC - http://msdn.microsoft.com/en-us/library/windows/desktop/dd369038(v=vs.85).aspx
  */
///////////////////////////////////////////////////////////////////////////////
/*
 *  ����� ���� ��� MS Windows.
 */
#ifndef WindowH
#define WindowH
//---------------------------------------------------------------------------
#include <windows.h>
#include "GLWidget.h"
#include "ModelPlot.h"

/// ���������, ���������� ����������� ���
/// �������� ���� ����
struct MainWindowData
{
	HWND hWnd;                             	// handle to window
    DWORD winStyle;                         // window style: WS_OVERLAPPEDWINDOW, WS_CHILD, ...
    DWORD winStyleEx;                       // extended window style
    LPWSTR title;              				// title of window
    LPWSTR className;          				// name of window class
    int x;                                  // window position X
    int y;                                  // window position Y
    int width;                              // window width
    int height;                             // window height
    HINSTANCE hInstance;                    // handle to instance
};

/// ���������, ���������� ����������� ���
/// �������� openGL ���� ����
struct GLWindowData
{
    HGLRC     hRC;		      // Permanent Rendering Context	// ���� ��� ����� OpenGL � �������
    HDC	      hDC;		      // Private GDI Device Context
    DWORD     dwExStyle;      // Window Extended Style
	DWORD     dwStyle;        // Window Style
    HWND      hWnd;		      // Holds Our Window Handle
    HINSTANCE hInstance;	  // Holds The Instance Of The GL window
    HWND      hWndParent;     // handle to parent window	   	// ��������� �� ���� ��������( MainWindowData)
    LPWSTR 	  title;
    LPWSTR 	  className;      // name of window class
    int x;                    // window position X
    int y;                    // window position Y
    int width;
    int height;
    int bits; //����� ��� �� ,,,,
};

struct MVData
{
	GLWidget  *view;
    ModelPlot *model;
};

class Window
{
	public:
		Window(GLWidget *view, ModelPlot *model, int width, int height, HINSTANCE hInstance, int nCmdShow);
		~Window(void);

                // �������� �������������
                int Create();
                // ���� � ���� ��������� ��������� �������
                int Exec();

        private:
                ///////////////////////////////////////////////////////////////
                // Main Window
                GLvoid KillMainWindow();
                BOOL CreateMainWindow();
                ///////////////////////////////////////////////////////////////
                // GL Window
        		GLvoid KillGLWindow();
				BOOL CreateGLWindow();
                ///////////////////////////////////////////////////////////////
                static LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
                ///////////////////////////////////////////////////////////////
        		HICON loadIcon(HINSTANCE hInstance,int id);         // load icon using resource id
        		HCURSOR loadCursor(HINSTANCE hInstance,int id);     // load icon using resource id
        private:
                MainWindowData wData;   		// ������ �������� ����
                GLWindowData glData;            // ������ open gl ����
                MVData data;                    // ��������� �� GLWidget � ModelPlot
                int cmdShow;                    // ��������������� �������� �� _tMain ��� ������� winapi ::ShowWindow(...);
				static bool keys[256];			// Array Used For The Keyboard Routine
	        	static bool active;				// Window Active Flag Set To TRUE By Default
};

#endif
