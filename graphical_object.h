//---------------------------------------------------------------------------

#ifndef graphical_objectH
#define graphical_objectH
#include <vector>
#include <map>
#include <string>
#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "bitfont/BitmapFont.h"
#include <iostream>

#include "txy.h"

using std::vector;
using std::string;
using std::cout;

struct Style {
	typedef enum {
		LINE_STD,
		LINE_DASHED,
		LINE_DOTTED,
		LINE_DASH_DOTTED
	} LineStroke;

	typedef enum {
		MARK_CROSS,
		MARK_SQUARE,
		MARK_DOT,
		MARK_TRIANGLE
	} MarkerType;

	typedef enum {
		TEXT_ALIGN_CENTER,
		TEXT_ALIGN_TOP,
		TEXT_ALIGN_BOTTOM,
		TEXT_ALIGN_LEFT,
		TEXT_ALIGN_RIGHT,
		TEXT_ALIGN_TOPLEFT,
		TEXT_ALIGN_TOPRIGHT,
		TEXT_ALIGN_BOTLEFT,
		TEXT_ALIGN_BOTRIGHT
	} TextAlignment;

	LineStroke lineStroke;
	unsigned int lineColor;
	float lineThickness;

	MarkerType markerType;
	unsigned int markerSize;
        unsigned int markerColor;

	TextAlignment textAlignment;
	size_t fontSize;
	string fontName;
        unsigned int textColor;
        unsigned int fillColor;
	Style(); 
};

class GraphicalObject {
	public:
	GraphicalObject();
	protected:
		bool isFixed;
		bool isScaled;
		bool isTranslated;

		double trX, trY;
		double scX, scY;

		void before_draw()const;
		void after_draw()const;
	public:
		virtual ~GraphicalObject(){};
		virtual void draw() const = 0;

		void setScale(double,double);
		void setTranslation(double,double);

		void toggleFixed(bool b = true);
		void toggleScaled(bool b = true);
		void toggleTranslated(bool b = true);

		Style style;
};

class PolylineS : public GraphicalObject {
	public:
		PolylineS(vector<Txy> *data);
		virtual void draw() const;
		virtual ~PolylineS(){cout << "Destroying polyline";};
	private:
		vector<Txy> *data;
};

class Line : public GraphicalObject {
	public:
		Line(double,double,double,double);
		virtual void draw() const;
		virtual ~Line(){};
		void setCoordinates(double,double,double,double);
	private:
		double x1,x2,y1,y2;
};

class Polygon : public GraphicalObject {
	public:
		Polygon(vector<Txy> *);
		virtual void draw() const;
		virtual ~Polygon(){};
	private:
		vector<Txy> *data;
};


typedef enum {
	MRK_TYPE_DOT,
	MRK_TYPE_CROSS
} MarkerType;

class MarkerSet : public GraphicalObject {
	public:
		MarkerSet (vector<Txy> *data);
		MarkerSet (vector<Txy> *data, vector<std::string> *tmarks);
		virtual ~MarkerSet (){ if(bf) delete bf;}

		virtual void draw() const;

	private:
		vector<Txy> *data;
		vector<std::string> *tmarks;
		BitmapFont *bf;

};

class Text : public GraphicalObject {
	public:
		Text(const string &text, size_t size, double x, double y);
		~Text(){};
		virtual void draw()const;

	private:
		double x, y;
                size_t size;
		string text;
		struct dtx_font *font;
};


#endif
