///////////////////////////////////////////////////////////////////////////////
/*
 *  ����� ���� ��� MS Windows.
 */
#pragma hdrstop
#include "Window.h"
///////////////////////////////////////////////////////////////////////////////
#pragma package(smart_init)

#define OFFSET 5;
#define OFFSETR 100;

bool Window::active = true;		// Window Active Flag Set To TRUE By Default
bool Window::keys[256];

Window::Window(GLWidget *view, ModelPlot *model, int width, int height, HINSTANCE hInstance, int nCmdShow) :
			   cmdShow(nCmdShow)
{
     data.view = view;
     data.model = model;

     wData.hWnd   = NULL;
     wData.title  = L"KbPlot";
     wData.className = L"KbPlot_Win_Class";
     wData.x 	  = 0;
     wData.y      = 0;
     wData.width  = width;
     wData.height = height;
     wData.hInstance = hInstance;
     glData.title  = L"KbPlot";
     glData.className = L"OpenGL_Win_KbPlot";
     glData.bits  = 16;
     glData.x	  = 0;
     glData.y     = 0;
     glData.width = width   - OFFSETR;
     glData.height = height - OFFSETR;
     glData.hInstance = hInstance;
}

Window::~Window()
{

}
///////////////////////////////////////////////////////////////////////////////
int Window::Create()
{
	bool success = data.view->GLinit();

    if (!success)
    {
        MessageBox(NULL,L"GL Initialization Failed.",L"ERROR",MB_OK|MB_ICONEXCLAMATION);
        return FALSE;
    }

	bool isWinMain = CreateMainWindow();
    bool isWinGL = CreateGLWindow();
    if (!isWinMain || !isWinGL)
    {
    	return FALSE;
    }

    // ������ ������ ���� � ������������ � ��� ����������
    RECT rect;
    ::GetWindowRect(glData.hWnd, &rect);      // get size of glWin
    rect.right += 80 + OFFSET;	// + ������ ������
    ::AdjustWindowRectEx(&rect, wData.winStyle, FALSE, wData.winStyleEx);
    ::SetWindowPos(wData.hWnd, 0, 100, 100, rect.right-rect.left, rect.bottom-rect.top, SWP_NOZORDER);
    //-------------------------

    ::ShowWindow(wData.hWnd,  cmdShow);
    ::ShowWindow(glData.hWnd, cmdShow);					// Show The Window
    ::SetForegroundWindow(glData.hWnd);				    // Slightly Higher Priority
    ::SetFocus(glData.hWnd);							// Sets Keyboard Focus To The Window

    return TRUE;
}
int Window::Exec()
{
	MSG msg;				   							// Windows Message Structure
	BOOL  done=FALSE;                        			// Bool Variable To Exit Loop
	while(!done)                                		// Loop That Runs Until done=TRUE
	{
    	if (PeekMessage(&msg,NULL,0,0,PM_REMOVE))   	// Is There A Message Waiting?
		{
			if (msg.message==WM_QUIT)					// Have We Received A Quit Message?
			{
				done=TRUE;                  			// If So done=TRUE
			}
			else                            			// If Not, Deal With Window Messages
			{
				TranslateMessage(&msg);             	// Translate The Message
				DispatchMessage(&msg);					// Dispatch The Message
			}
		}
		else                                			// If There Are No Messages
		{
			// Draw The Scene.  Watch For ESC Key And Quit Messages From DrawGLScene()
			if (active)                     // Program Active?
			{
				if (keys[VK_ESCAPE])        // Was ESC Pressed?
				{
					done=TRUE;              // ESC Signalled A Quit
				}
				else                        // Not Time To Quit, Update Screen
				{
					data.view->GLpaint();          // Draw The Scene
					SwapBuffers(glData.hDC);       // Swap Buffers (Double Buffering)
				}
			}
		}
	}
	KillGLWindow();									// Kill The GL Window
    KillMainWindow();                               // Kill The Main Window
    return (msg.wParam);                            // Exit The Program
}
///////////////////////////////////////////////////////////////////////////////
// Main Window
///////////////////////////////////////////////////////////////////////////////
GLvoid Window::KillMainWindow()
{
	::UnregisterClass(wData.className, wData.hInstance);
    wData.hInstance = NULL;
}
///////////////////////////////////////////////////////////////////////////////
BOOL Window::CreateMainWindow()
{
     wData.hWnd 	  = 0;
     wData.winStyle   = WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN;
     wData.winStyleEx = WS_EX_WINDOWEDGE;
     wData.x 		  = CW_USEDEFAULT;
     wData.y 		  = CW_USEDEFAULT;
     wData.width 	  = CW_USEDEFAULT;
     wData.height 	  = CW_USEDEFAULT;

	WNDCLASSEX winClass;                    								// window class information
    // populate window class struct
    winClass.cbSize        = sizeof(WNDCLASSEX);
    winClass.style         = 0;                                     	    // class styles: CS_OWNDC, CS_PARENTDC, CS_CLASSDC, CS_GLOBALCLASS, ...
    winClass.lpfnWndProc   = (WNDPROC)WndProc;                  		    // pointer to window procedure
    winClass.cbClsExtra    = 0;
    winClass.cbWndExtra    = 0;
    winClass.hInstance     = wData.hInstance;                              	// owner of this class
    winClass.hIcon         = LoadIcon(wData.hInstance, IDI_APPLICATION);    // default icon
    winClass.hIconSm       = 0;
    winClass.hCursor       = LoadCursor(0, IDC_ARROW);              		// default arrow cursor
    winClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);   		// default white brush
    winClass.lpszMenuName  = 0;
    winClass.lpszClassName = wData.className;
    winClass.hIconSm       = LoadIcon(wData.hInstance, IDI_APPLICATION);    // default small icon

    // register a window class
    if(!::RegisterClassEx(&winClass))
    {
        MessageBox(NULL,L"Failed To Register The Window Class.",L"ERROR",MB_OK|MB_ICONEXCLAMATION);
        return FALSE;
    }


    wData.hWnd = ::CreateWindowEx(
    							wData.winStyleEx,           // window border with a sunken edge
                              	wData.className,            // name of a registered window class
                              	wData.title,                // caption of window
                              	wData.winStyle,             // window style
                              	wData.x,                    // x position
                              	wData.y,                    // y position
                              	wData.width,                // witdh
                              	wData.height,               // height
                              	0,         	    			// handle to parent window
                              	0,           				// handle to menu
                              	wData.hInstance,            // application instance
                                (LPVOID)&data);    		    // window creation data

    if(!wData.hWnd)
    {
    	KillMainWindow();
        MessageBox(NULL,L"Window Creation Error.",L"ERROR",MB_OK|MB_ICONEXCLAMATION);
        return FALSE;
    }

     // �������� 3-� �����������.
     const int posX = glData.width + OFFSET;
     const posY = (int)OFFSET;
   CreateWindow(L"button", L"Option 1",
      WS_CHILD|WS_VISIBLE|BS_AUTORADIOBUTTON/*BS_RADIOBUTTON*/ ,
      posX, posY, 80, 30, wData.hWnd, (HMENU)10001, wData.hInstance, NULL);
   CreateWindow(L"button", L"Option 2",
      WS_CHILD|WS_VISIBLE|BS_AUTORADIOBUTTON ,
      posX, posY + 30, 80, 30, wData.hWnd, (HMENU)10002, wData.hInstance, NULL);
   CreateWindow(L"button", L"Option 3",
      WS_CHILD|WS_VISIBLE|BS_AUTORADIOBUTTON ,
      posX, posY + 60, 80, 30, wData.hWnd, (HMENU)10003, wData.hInstance, NULL);
   CreateWindow(L"button", L"Option 4",
      WS_CHILD|WS_VISIBLE|BS_AUTORADIOBUTTON,
      posX, posY + 90, 80, 30, wData.hWnd, (HMENU)10004, wData.hInstance, NULL);

   return TRUE;
}
///////////////////////////////////////////////////////////////////////////////
// GL Window
///////////////////////////////////////////////////////////////////////////////
// Properly Kill The Window
GLvoid Window::KillGLWindow(GLvoid)
{
	if (glData.hRC)								// Do We Have A Rendering Context?
	{
    	glFinish();
		if (!wglMakeCurrent(NULL,NULL))     	// Are We Able To Release The DC And RC Contexts?
			MessageBox(NULL,L"Release Of DC And RC Failed.",L"SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);

		if (!wglDeleteContext(glData.hRC))      // Are We Able To Delete The RC?
			MessageBox(NULL,L"Release Rendering Context Failed.",L"SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);

		glData.hRC = NULL;                      // Set RC To NULL
	}

	if (glData.hDC && !ReleaseDC(glData.hWnd, glData.hDC))			// Are We Able To Release The DC
	{
		MessageBox(NULL,L"Release Device Context Failed.",L"SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		glData.hDC=NULL;											// Set DC To NULL
	}

	if (glData.hWnd && !DestroyWindow(glData.hWnd))       			// Are We Able To Destroy The Window?
	{
		MessageBox(NULL,L"Could Not Release hWnd.",L"SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		glData.hWnd=NULL;                          					// Set hWnd To NULL
	}

	if (!UnregisterClass(glData.className, glData.hInstance))   	// Are We Able To Unregister Class
	{
		MessageBox(NULL,L"Could Not Unregister Class.",L"SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
        glData.hInstance=NULL;                         				// Set hInstance To NULL
    }
}
///////////////////////////////////////////////////////////////////////////////
BOOL Window::CreateGLWindow()
{
	GLuint      PixelFormat;					// Holds The Results After Searching For A Match
	WNDCLASS  wc;	     						// Windows Class Structure


	RECT WindowRect;                            // Grabs Rectangle Upper Left / Lower Right Values
	WindowRect.left   =(long)glData.x;          // Set Left Value To 0
	WindowRect.right  =(long)glData.width;      // Set Right Value To Requested Width
	WindowRect.top	  =(long)glData.y;          // Set Top Value To 0
	WindowRect.bottom =(long)glData.height;     // Set Bottom Value To Requested Height

	glData.hInstance = GetModuleHandle(NULL);  			// Grab An Instance For Our Window
	wc.style  = CS_OWNDC;
	wc.lpfnWndProc = (WNDPROC)WndProc;					// WndProc Handles Messages
	wc.cbClsExtra = 0;									// No Extra Window Data
	wc.cbWndExtra = 0;									// No Extra Window Data
	wc.hInstance = glData.hInstance;				   	// Set The Instance
	wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);				// Load The Default Icon
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);			// Load The Arrow Pointer
	wc.hbrBackground = NULL;							// No Background Required For GL
	wc.lpszMenuName = NULL;								// We Don't Want A Menu
	wc.lpszClassName = glData.className;   					// Set The Class Name

	if (!RegisterClass(&wc))							// Attempt To Register The Window Class
	{
		MessageBox(NULL,L"Failed To Register The Window Class.",L"ERROR",MB_OK|MB_ICONEXCLAMATION);
		return FALSE;									// Exit And Return FALSE
	}

    glData.dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;   // Window Extended Style
    glData.dwStyle   = WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;                  // Windows Style

	if (!(glData.hWnd=CreateWindowEx(
    								glData.dwExStyle,        			// Extended Style For The Window
        							glData.className,		 			// Class Name
									glData.title,  			 			// Window Title
									WS_CLIPSIBLINGS |					// Required Window Style
									WS_CLIPCHILDREN |				 	// Required Window Style
									glData.dwStyle,				   		// Selected Window Style
                					glData.x, glData.y,		  			// Window Position
									WindowRect.right-WindowRect.left,	// Calculate Adjusted Window Width
                					WindowRect.bottom-WindowRect.top,	// Calculate Adjusted Window Height
                					wData.hWnd,	   						// Parent Window
                                    NULL,								// No Menu
                					glData.hInstance,					// Instance
									(LPVOID)&data)))

				{
					KillGLWindow();                         // Reset The Display
					MessageBox(NULL,L"Window Creation Error.",L"ERROR",MB_OK|MB_ICONEXCLAMATION);
					return FALSE;                           // Return FALSE
				}

				static  PIXELFORMATDESCRIPTOR pfd=                  // pfd Tells Windows How We Want Things To Be
				{
					sizeof(PIXELFORMATDESCRIPTOR),                  // Size Of This Pixel Format Descriptor
					1,												// Version Number
					PFD_DRAW_TO_WINDOW |							// Format Must Support Window
					PFD_SUPPORT_OPENGL |							// Format Must Support OpenGL
					PFD_DOUBLEBUFFER,								// Must Support Double Buffering
					PFD_TYPE_RGBA,									// Request An RGBA Format
					glData.bits,											// Select Our Color Depth
					0, 0, 0, 0, 0, 0,								// Color Bits Ignored
					0,												// No Alpha Buffer
					0,												// Shift Bit Ignored
					0,												// No Accumulation Buffer
					0, 0, 0, 0,										// Accumulation Bits Ignored
					16,												// 16Bit Z-Buffer (Depth Buffer)
					0,												// No Stencil Buffer
					0,												// No Auxiliary Buffer
					PFD_MAIN_PLANE,									// Main Drawing Layer
					0,												// Reserved
					0, 0, 0											// Layer Masks Ignored
				};

				if (!(glData.hDC=GetDC(glData.hWnd)))  	 			// Did We Get A Device Context?
				{
					KillGLWindow();									// Reset The Display
					MessageBox(NULL,L"Can't Create A GL Device Context.",L"ERROR",MB_OK|MB_ICONEXCLAMATION);
					return FALSE;									// Return FALSE
				}

				if (!(PixelFormat=ChoosePixelFormat(glData.hDC,&pfd)))	// Did Windows Find A Matching Pixel Format?
				{
					KillGLWindow();									// Reset The Display
					MessageBox(NULL,L"Can't Find A Suitable PixelFormat.L",L"ERROR",MB_OK|MB_ICONEXCLAMATION);
					return FALSE;									// Return FALSE
				}

				if(!SetPixelFormat(glData.hDC,PixelFormat,&pfd))    // Are We Able To Set The Pixel Format?
				{
					KillGLWindow();									// Reset The Display
					MessageBox(NULL,L"Can't Set The PixelFormat.",L"ERROR",MB_OK|MB_ICONEXCLAMATION);
					return FALSE;									// Return FALSE
				}

				if (!(glData.hRC=wglCreateContext(glData.hDC)))     // Are We Able To Get A Rendering Context?
				{
					KillGLWindow();									// Reset The Display
					MessageBox(NULL,L"Can't Create A GL Rendering Context.",L"ERROR",MB_OK|MB_ICONEXCLAMATION);
					return FALSE;									// Return FALSE
				}

				if(!wglMakeCurrent(glData.hDC, glData.hRC))         // Try To Activate The Rendering Context
				{
					KillGLWindow();									// Reset The Display
					MessageBox(NULL,L"Can't Activate The GL Rendering Context.",L"ERROR",MB_OK|MB_ICONEXCLAMATION);
					return FALSE;									// Return FALSE
				}

				return TRUE;										// Success
}
///////////////////////////////////////////////////////////////////////////////
LRESULT CALLBACK Window::WndProc(   HWND    hWnd,       // Handle For This Window
                UINT    uMsg,                   // Message For This Window
                WPARAM  wParam,                 // Additional Message Information
                LPARAM  lParam)                 // Additional Message Information
{

	// find controller associated with window handle
	static MVData *data;
	data = (MVData*)::GetWindowLongPtr(hWnd, GWL_USERDATA);

    // �������� ��������� MVData* c ����������� �� GLWidget � ModelPlot
    if(uMsg == WM_NCCREATE)  // Non-Client Create
    {
        // WM_NCCREATE message is called before non-client parts(border,
        // titlebar, menu,etc) are created. This message comes with a pointer
        // to CREATESTRUCT in lParam. The lpCreateParams member of CREATESTRUCT
        // actually contains the value of lpPraram of CreateWindowEX().
        // First, retrieve the pointrer to the controller specified when
        // Win::Window is setup.
		data = (MVData*)(((CREATESTRUCT*)lParam)->lpCreateParams);
		if(data)
		{
			// Store the pointer to the GLWidget into GWL_USERDATA,
			// so, other messege can be routed to the associated Controller.
			::SetWindowLongPtr(hWnd, GWL_USERDATA, (LONG_PTR)data);
		}
		else
			MessageBox(NULL,L"[ERROR] Failed to create controller::windowProcedure().",L"",MB_OK|MB_ICONQUESTION);

        return ::DefWindowProc(hWnd, uMsg, wParam, lParam);
    }

    // check NULL pointer, because GWL_USERDATA is initially 0, and
    // we store a valid pointer value when WM_NCCREATE is called.
    if(!data)
        return ::DefWindowProc(hWnd, uMsg, wParam, lParam);

	switch (uMsg)                               // Check For Windows Messages
	{
    	case WM_COMMAND:
        data->view->clearVector();
        // ���� �� ������ �� 1-� �����������.
      	if(LOWORD(wParam)==10001)
        	data->model->CreateOption1();
      	// ���� �� ������ �� 2-� �����������.
      	if(LOWORD(wParam)==10002)
         	data->model->CreateOption2();
      	// ���� �� ������ �� 3-� �����������.
      	if(LOWORD(wParam)==10003)
         	data->model->CreateOption3();
        if(LOWORD(wParam)==10004)
         	data->model->CreateOption4();
        /*
 		// ���������, �� �������� �� �� ������ ������ �����������.
      	if(10001<= LOWORD(wParam) && LOWORD(wParam)<=10002)
      	{
      	   	// �������� ������ �� ������ ������.
           	CheckRadioButton(hWnd, 10001, 10002, LOWORD(wParam));
            switch(LOWORD(wParam))
            {
                case 10001:
                data->model->CreateOption1();
                break;
            	case 10002:
                data->model->CreateOption2();
                break;
            }
      	}
     	 // ���������, �� �������� �� �� ������ ������ �����������.
      	if(10003<= LOWORD(wParam) && LOWORD(wParam)<=10004)
      	{
       	  	// �������� ������ �� ������ ������.
      	   	CheckRadioButton(hWnd, 10003, 10004, LOWORD(wParam));
            switch(LOWORD(wParam))
            {
                case 10003:
                data->model->CreateOption3();
                break;
            	case 10004:
                data->model->CreateOption4();
                break;
            }
      	}
        break;
        */
		case WM_ACTIVATE:                       // Watch For Window Activate Message
		{
			// LoWord Can Be WA_INACTIVE, WA_ACTIVE, WA_CLICKACTIVE,
			// The High-Order Word Specifies The Minimized State Of The Window Being Activated Or Deactivated.
			// A NonZero Value Indicates The Window Is Minimized.
			if ((LOWORD(wParam) != WA_INACTIVE) && !((BOOL)HIWORD(wParam)))
				active=TRUE;						// Program Is Active
			else
				active=FALSE;						// Program Is No Longer Active

			return 0;								// Return To The Message Loop
		}

		case WM_SYSCOMMAND:                     // Intercept System Commands
		{
			switch (wParam)                     // Check System Calls
			{
				case SC_SCREENSAVE:             // Screensaver Trying To Start?
				case SC_MONITORPOWER:           // Monitor Trying To Enter Powersave?
				return 0;						// Prevent From Happening
			}
			break;								// Exit
		}

		case WM_CLOSE:                          // Did We Receive A Close Message?
		{
			PostQuitMessage(0);                 // Send A Quit Message
			return 0;							// Jump Back
		}

		case WM_SIZE:                           // Resize The OpenGL Window
		{
			// LoWord=Width, HiWord=Height
            int width = LOWORD(lParam);
            int height = HIWORD(lParam);
            if (height==0)				  // Prevent A Divide By Zero By
            	height=1;
            data->view->GLresize(width, height);
			return 0;							// Jump Back
		}

		case WM_LBUTTONDOWN:
		{
        	short xPos = LOWORD(lParam);
			short yPos = HIWORD(lParam);
			data->view->mousePressedEvent(xPos, yPos);
			return 0;
		}

		case WM_LBUTTONUP:
		{
			short xPos = LOWORD(lParam);
			short yPos = HIWORD(lParam);
			data->view->mouseReleaseEvent(xPos, yPos);
			return 0;
		}
		case WM_MOUSEMOVE:
		{
			short xPos = LOWORD(lParam);
			short yPos = HIWORD(lParam);
			data->view->mouseMoveEvent(xPos, yPos);
			return 0;
		}
		case WM_MOUSEWHEEL:
		{
			int zDelta = GET_WHEEL_DELTA_WPARAM(wParam);
			data->view->wheelEvent(zDelta);
			return 0;
        }
	}
	 // Pass All Unhandled Messages To DefWindowProc
    return DefWindowProc(hWnd,uMsg,wParam,lParam);
}
///////////////////////////////////////////////////////////////////////////////
// load an icon using resource ID and convert it to HICON
///////////////////////////////////////////////////////////////////////////////
HICON Window::loadIcon(HINSTANCE hInstance,int id)
{
    return (HICON)::LoadImage(hInstance, MAKEINTRESOURCE(id), IMAGE_ICON, 0, 0, LR_DEFAULTSIZE);
}
///////////////////////////////////////////////////////////////////////////////
// load an icon using resource ID and convert it to HICON
///////////////////////////////////////////////////////////////////////////////
HICON Window::loadCursor(HINSTANCE hInstance,int id)
{
    return (HCURSOR)::LoadImage(hInstance, MAKEINTRESOURCE(id), IMAGE_CURSOR, 0, 0, LR_DEFAULTSIZE);
}
