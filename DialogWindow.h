//---------------------------------------------------------------------------

#ifndef DialogWindowH
#define DialogWindowH
//---------------------------------------------------------------------------
	public:
		Window(GLWidget *gl);
		~Window(void);

                void SetPosition(int x, int y, int cx, int cy, unsigned int flags);
                int Create();
                int Exec();
        private:

        	GLvoid KillGLWindow(GLvoid);
		BOOL CreateGLWindow(LPWSTR title, int width, int height, int bits, bool fullscreenflag);
		static LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);		// Declaration For WndProc
        private:

        	HGLRC	  hRC;							// Permanent Rendering Context
		HDC	  hDC;							// Private GDI Device Context
		HWND	  hWnd;							// Holds Our Window Handle
		HINSTANCE hInstance;						// Holds The Instance Of The Application

		GLWidget *gl;

		bool keys[256];												// Array Used For The Keyboard Routine
	        static bool active;		// Window Active Flag Set To TRUE By Default

#endif
