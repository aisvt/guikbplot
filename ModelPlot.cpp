//---------------------------------------------------------------------------

#pragma hdrstop

#include "ModelPlot.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

void ModelPlot::Clear()
{
     if(v.size() > 0)
     	v.clear();
     if(ds)  delete ds;
     if(ds1) delete ds1;
     if(s)   delete s;
     if(s1)  delete s1;
     if(p)   delete p;
	 if(strM.size() > 0)
     	strM.clear();
}

void ModelPlot::CreateOption1()
{
	Clear();

	p = new KbPlot(gl, -10.0, 50.0, -1.0, 50.0);

	v.push_back(Txy(0.0,0.0));
    v.push_back(Txy(1.0,1.0));
    v.push_back(Txy(-1.0,1.0));
    v.push_back(Txy(2.0,4.0));
    v.push_back(Txy(-2.0,4.0));
    v.push_back(Txy(3.0,9.0));
	v.push_back(Txy(-3.0,9.0));

    ds = new DataSet(&v);

    double data[256];
    for (int i = 0; i < 128; i+=2) {
    	data[i] = i;
    	// data[i+1] = log(i*i*i);
    	data[i+1] = (i*i*i);
    }
    ds1 = new DataSet(RawData::get((unsigned char*)(&data[2]),50, 16, 0, 8, RawData::TYPE_DOUBLE));
    s = new Style();
    s1 = new Style();
    s->lineColor = 0x00FF00FF;
    s1->lineColor = 0x6555FFFF;  //
    s->lineThickness = 1.0;
	s1->lineThickness = 3.0;
    s->markerSize = 20;
    s->markerColor = 0xFF000055;
    s->markerType = Style::MARK_CROSS;
    s1->lineStroke = Style::LINE_DASHED;
    s->lineStroke = Style::LINE_DASH_DOTTED;
    p->toggleGrid(true);
    p->setGridColor(0x77777777);
    p->setGridYStroke(Style::LINE_DOTTED);
	strM.push_back("1");
	strM.push_back("2");
	strM.push_back("3");
	strM.push_back("4");
	strM.push_back("5");
	strM.push_back("6");
	strM.push_back("7");
	p->draw(*ds1, strM, *s1);
	p->draw(*ds , strM, *s);
}

void ModelPlot::CreateOption2()
{
	Clear();

	p = new KbPlot(gl, -10.0, 50.0, -1.0, 50.0);

	v.push_back(Txy(0.0,0.0));
    v.push_back(Txy(1.0,1.0));
    v.push_back(Txy(-1.0,1.0));
    v.push_back(Txy(2.0,4.0));
    v.push_back(Txy(-2.0,4.0));
    v.push_back(Txy(3.0,9.0));
	v.push_back(Txy(-3.0,9.0));

    ds = new DataSet(&v);

    double data[256];
    for (int i = 0; i < 128; i+=2) {
    	data[i] = i;
    	// data[i+1] = log(i*i*i);
    	data[i+1] = (i*i*i);
    }
    ds1 = new DataSet(RawData::get((unsigned char*)(&data[2]),50, 16, 0, 8, RawData::TYPE_DOUBLE));
    s = new Style();
    s1 = new Style();
    s->lineColor = 0x00FF00FF;
    s1->lineColor = 0x6555FFFF;  //
    s->lineThickness = 1.0;
	s1->lineThickness = 3.0;
    s->markerSize = 20;
    s->markerColor = 0xFF000055;
    s->markerType = Style::MARK_SQUARE;
    s1->lineStroke = Style::LINE_DASHED;
    s->lineStroke = Style::LINE_DASH_DOTTED;
    p->toggleGrid(true);
    p->setGridColor(0x77777777);
    p->setGridYStroke(Style::LINE_DOTTED);
	strM.push_back("1");
	strM.push_back("2");
	strM.push_back("3");
	strM.push_back("4");
	strM.push_back("5");
	strM.push_back("6");
	strM.push_back("7");
	p->draw(*ds1, strM, *s1);
	p->draw(*ds , strM, *s);
}

void ModelPlot::CreateOption3()
{

}

void ModelPlot::CreateOption4()
{
	
}