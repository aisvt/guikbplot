//---------------------------------------------------------------------------

#include "glwin.h"
#include "kbplot.h"
#pragma hdrstop

#include "Window.h"
#include "ModelPlot.h"

#include <tchar.h>
#include <math.h>
#include <stdlib.h>

#pragma argsused

WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow)
{
	GLWidget gl;
    ModelPlot plot(&gl);
    Window app(&gl, &plot, 640, 480, hInstance, nCmdShow);
    app.Create();
    return app.Exec();//���� � ���� ������������ ���������
}



