
#ifndef IglwidgetH
#define IglwidgetH


#include <windows.h>
#include <GL/GL.h>



struct IGLWidget
{
	virtual void GLpaint() = 0;
	virtual void GLresize(int nw, int nh) = 0;
	virtual void GLupdate() = 0;
	virtual bool GLinit() = 0;
	virtual void mouseMoveEvent(int xPos, int yPos) = 0;
	virtual void mousePressedEvent(int xPos, int yPos) = 0;
	virtual void mouseReleaseEvent(int xPos, int yPos) = 0;
	virtual void wheelEvent(int delta) = 0;
};

IGLWidget *CreateGLWidget();

#endif

